
/**
 * Created by webs on 10.04.15.
 */

var gulp = require('gulp');
var config = {};
gulp.pm = {};
gulp.pm.chalk = require('chalk');
gulp.pm.uglify     = require('gulp-uglify');
gulp.pm.concatJs   = require('gulp-concat');

gulp.pm.del = require('del');
gulp.pm.rename = require('gulp-rename');
gulp.pm.prefix = require('gulp-autoprefixer');
gulp.pm.minifyCSS  = require('gulp-minify-css');
gulp.pm.sourcemaps = require('gulp-sourcemaps');
gulp.pm.concatCss  = require('gulp-concat-css');
gulp.pm.browserSync = require('browser-sync').create();
var beautify_js = require('js-beautify');
var beautify_css = require('js-beautify').css;
gulp.pm.includer     = require('gulp-x-includer');

var sass = require('gulp-sass');
var browserify   = require('browserify');
var autopolyfiller = require('gulp-autopolyfiller');
var order = require('gulp-order');
var merge = require('event-stream').merge;
var vinylSource  = require('vinyl-source-stream');
var vinylBuffer  = require('vinyl-buffer');
var extractor    = require('gulp-extract-sourcemap');
var uglify       = require('gulp-uglifyjs');
var filter       = require('gulp-filter');
var program = require('commander');
var fs = require('fs');

// настройки gulp
config = {
    // common
    pathGulpModules: './gulp_modules/',
    pathRoot:'public/',
    pathDest: 'public/bundle/',
    pathHtmlDest: 'public/',
    minifyCss: false,
    minifyJs: true, //
    filenameStyle: 'style',                     // style.min.css, style.css
    filenameJavascript: 'javascript',           // javascript.min.js, javascript.js
    filenameJavascriptPolyfill: 'jspolyfill',           // polyfill.min.js, polyfill.js
    filenameStyleLibs: 'style.libs',            // style.libs.min.css, style.libs.css
    filenameJavascriptLibs: 'javascript.libs',  // javascript.libs.min.js, javascript.libs.js
    // browserify section
    shimsFileSrc: '../shims', // файл shims.js с описанием вендорных библиотек
    mainFile: './public/app/js/main.js', // главный файл(точка входа)
    // Sass
    pathSassSrc: 'public/app/styles/sass/*.sass',
    pathSassMainSrc: 'public/app/styles/sass/main.sass',
    // Scss
    pathScssSrc: 'public/app/styles/scss/**/*.scss',
    pathScssMainSrc: 'public/app/styles/scss/main.scss',
    // stylus
    pathStylusSrc: 'public/app/styles/**/*.styl',
    // css
    pathCssSrc: 'public/app/styles/css/*.css',
    // javascript es5
    pathJsSrc: 'public/app/js/**/*.js',
    pathJsES6Src: 'public/app/js/**/*.es6.js',
    pathHtmlSrc: 'public/app/*.html',
    // глобальное подключение библиотек
    bower: {
        styles: [
            'bower_components/normalize-css/normalize.css',
            'bower_components/foundation/css/foundation.css',

        ],
        javascript: [
            'bower_components/jquery/dist/jquery.js',
            'bower_components/jquery.mobile.custom/jquery.mobile.custom.min.js',
            'bower_components/modernizr/modernizr.js',
            'bower_components/matreshka/matreshka.min.js'
        ]
    }
};
gulp.pm.config = config;
var config = gulp.pm.config;
// commandline
program
    .version('0.0.1')
    .option('--silent', 'Отключение логов')
    .option('-s, --minifyCSS', 'Минифицировать css')
    .parse(process.argv);

if (program.minifyCSS) {
    gulp.pm.config.minifyCss = true;
    // message
    var message = gulp.pm.chalk.yellow('+----')
    message += gulp.pm.chalk.black.bold('Минификация стилей ВКЛ')
    message += gulp.pm.chalk.yellow('----+')
    console.info(message);
};

module.exports = gulp;

// подключение модулей задачь
require(config.pathGulpModules + 'bower_libs_module');
require(config.pathGulpModules + 'html_module');
require(config.pathGulpModules + 'server_module');

// дефолтная задача
gulp.task('default', ['server'], function (cb) {

    var message = gulp.pm.chalk.green('+-------------------------------+\n');
        message += gulp.pm.chalk.green.bold('Запущен сервер, по адресу указанном в низу можно открыть в браузере и посмотреть результат \n');
        message += gulp.pm.chalk.green('+-------------------------------+\n');

    console.info(message);
});

gulp.task('watch', ['server'], function () {

    gulp.watch(gulp.pm.config.pathHtmlSrc, function () {
        var pathDest = config.pathHtmlDest || 'public/';

        gulp.src(config.pathHtmlSrc)
            .pipe(gulp.pm.includer())
            .pipe(gulp.dest(pathDest))

        var message = gulp.pm.chalk.gray('+-------------------------------+\n')
        message += gulp.pm.chalk.black.bold('Задача html выполнена\n')
        message += gulp.pm.chalk.gray('+-------------------------------+')
        console.info(message);
    });

    gulp.watch(gulp.pm.config.pathScssSrc, function () {
        'use strict';
        var pathDest = config.pathScssDest || config.pathDest;

        var pathFilenameStyleMin = pathDest + config.filenameStyle + '.min.css';
        var pathFilenameStyle = pathDest + config.filenameStyle + '.css';
        var pathFilenameStyleMain = pathDest  + 'main.css';

        var task = gulp.src(config.pathScssMainSrc)
         //  .pipe(gulp.pm.sourcemaps.init())
            .pipe(sass())
            .pipe(gulp.pm.prefix('last 2 version', 'ff', 'op_mini', 'and_chr', 'and_ff', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))

        var task_minfy = task;
        task.pipe(gulp.pm.rename(config.filenameStyle + '.min.css'))
            /*.pipe(gulp.dest(pathDest))
            .on('end', function () {
                fs.readFile(pathFilenameStyleMin, 'utf8', function (err, data) {
                    if (!err) {
                        fs.writeFile(pathFilenameStyle, beautify_css(data, {indent_size: 2}), function (err) {
                            if (err) return console.log(err);
                        });
                    }
                });
            });*/

        task_minfy
            .pipe(gulp.pm.minifyCSS({keepBreaks:false, keepSpecialComments:0, cache: true}))
            //.pipe(gulp.pm.sourcemaps.write())
            .pipe(gulp.dest(pathDest));

        var message = gulp.pm.chalk.gray('+-------------------------------+\n')
        message += gulp.pm.chalk.black.bold('Задача scss выполнена\n')
        message += gulp.pm.chalk.gray('+-------------------------------+')
        console.info(message);
    });

    gulp.watch(gulp.pm.config.pathJsSrc, function () {
        'use strict';
        var pathDest = config.pathJsDest || config.pathDest;
        var pathMapFile = pathDest + config.filenameJavascript + '.js.map';

        var sourcemap = '';
        var exchange = {
            source_map: {
                root: '/',
                orig: ''
            }
        };

        var all = browserify({
            paths:'public/app/js',
            commondir:      true,
            insertGlobals:  true,
            debug: true
        })
            .add(config.mainFile)
            .bundle()
            .pipe(vinylSource(config.filenameJavascript + '.js'))
            .pipe(vinylBuffer())
            .pipe(extractor({
                basedir:pathDest,
                removeSourcesContent: true,
                sourceMappingFileName: config.filenameJavascript + '.js.map'
            }))
            .on('postextract', function (sourceMap) {
                exchange.source_map.orig = sourceMap;
            })
            .pipe(filter('**/*.js'))

        var sourcemap = all;

        var polyfills = all.pipe(autopolyfiller(config.filenameJavascriptPolyfill + '.js', {
            browsers: ['last 2 version', 'ie 9']
        }))
        //.pipe(gulp.pm.uglify());

        var js = merge(polyfills, all)
            .pipe(order([
                config.filenameJavascriptPolyfill + '.js',
                config.filenameJavascript + '.js'
            ]));

        if (config.minifyJs === true) {
            js.pipe(uglify(config.filenameJavascript + '.min.js', {
                outSourceMap: true,
                basePath: pathDest,
                output: {source_map: exchange.source_map}
            })).pipe(gulp.dest(pathDest))
        } else {
            js.pipe(gulp.pm.concatJs(config.filenameJavascript + '.js'))
                .pipe(gulp.dest(pathDest));

        }

        var message = gulp.pm.chalk.gray('+-------------------------------+\n')
        message += gulp.pm.chalk.black.bold('Задача browserify выполнена\n')
        message += gulp.pm.chalk.gray('+-------------------------------+')
        console.info(message);
    });

    gulp.watch(['public/bundle/*.js', 'public/bundle/*.css', 'public/*.html'])
        .on('change', gulp.pm.browserSync.reload);
});


