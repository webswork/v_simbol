#Символ: репозитарий на https://bitbucket.org/webswork/v_simbol



# Faster
Простой инструмент на основе gulp'а для верстки.

### Зависимости: (это должно уже установлено если нет добропожаловать [ниже][для_них] (установка сервера…)
```
сервер: nodejs или iojs
менеджер пакетов: bower
менеджер задач: gulp
```
Расширение Livereload для [chrome](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?utm_source=chrome-app-launcher-info-dialog) [firefox](https://addons.mozilla.org/ru/firefox/addon/livereload/?src=search)


### Установка: 
```
1. $ git clone https://github.com/weber/faster.git( ну или качаем [zip](https://github.com/weber/faster/archive/master.zip)) :)
2. $ cd faster/
3. $ npm install
4. $ bower install 
```
### Запуск:
```
$ gulp watch
```
Открываем в браузере http://localhost:3000
Работает совместно с browsersync, :)


### Структура:
bower_components - клиентские библиотеки проекта(ну тут все ясно) 
gulp_modules - директория с файлами задач gulp'а 
public - директория проекта :)
.bowerrc - указываем где будем хранить ресурсы bower'а 
bower.json - файл настройки (указываем какие либы будем использовать jquery, underscore…)
gulpfile.js - главный файл он же файл настройки
package.json - настройки (при использование browserify указываем в разделе «browser» какие библиотеки будем использовать)

[для_них]: ""
### 1. Установка сервера:
##### Ubuntu 
##### nodejs
```
//стандарт
sudo apt-get install nodejs
sudo apt-get install npm
```
или

```
// ppa
apt-get install python-software-properties
apt-add-repository ppa:chris-lea/node.js
apt-get update
sudo apt-get install nodejs
sudo apt-get install npm
```
или 
##### iojs
```
//(32bit)iojs-v1.8.1-linux-x86.tar.gz 
// (64bit)
wget https://iojs.org/dist/v1.8.1/iojs-v1.8.1-linux-x64.tar.gz 
tar zxf iojs-v1.8.1-linux-x64.tar.gz 
cd iojs-v1.0.2-linux-x64
cp bin/* /usr/bin
```

##### Windows
(для великих)
[Пошаговая установка Node.js на Windows без виртуалок](http://habrahabr.ru/post/107232/)

### 2. Установка менеджер пакетов:
##### Ubuntu 
```
$ npm install -g bower
```
### 3. Установка менеджер задач:
##### Ubuntu 
```
$ npm install -g gulp
```

MIT License
===========

    Copyright (C) 2015 by Sergey Kirichenko

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN