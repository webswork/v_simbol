/**
 * Created by webs on 10.04.15.
 */

var gulp = require('../gulpfile');
var config = gulp.pm.config;
var nib = require('nib');
var stylus = require('gulp-stylus');

gulp.task('stylus', function(cb) {

    pathDest = config.pathStylusDest || config.pathDest;

    var pathFilenameStyleMin = pathDest+ config.filenameStyle + '.min.css';
    var pathFilenameStyle = pathDest + config.filenameStyle + '.css';
    gulp.pm.del([pathFilenameStyleMin, pathFilenameStyle],{force:true}, cb);

    var task =  gulp.src(config.pathStylusSrc)
        .pipe(gulp.pm.sourcemaps.init())
        .pipe(stylus({use: [nib()]}))
        .pipe(gulp.pm.prefix('last 2 version', 'ff', 'op_mini', 'and_chr', 'and_ff', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))

    if (config.minifyCss === true) {
        task.pipe(gulp.pm.rename(config.filenameStyle + '.min.css'))
            .pipe(gulp.dest(pathDest))
            .pipe(gulp.pm.minifyCSS({keepBreaks:false,keepSpecialComments:0,cache: true}))
            .pipe(gulp.dest(pathDest))
            .pipe(gulp.pm.sourcemaps.write())
            .pipe(gulp.dest(pathDest))
    } else {
        task.pipe(gulp.pm.rename(config.filenameStyle + '.css'))
        task.pipe(gulp.dest(pathDest))
            .pipe(gulp.pm.sourcemaps.write())
            .pipe(gulp.dest(pathDest))
    }

    gulp.src(config.pathDest + '*.css')//.pipe(gulp.pm.livereload());

    var message = gulp.pm.chalk.gray('+-------------------------------+\n')
    message += gulp.pm.chalk.black.bold('Задача stylus выполнена\n')
    message += gulp.pm.chalk.gray('+-------------------------------+')
    console.info(message);
});

