/**
 * Created by webs on 12.04.15.
 */
var gulp = require('../gulpfile');
var config = gulp.pm.config;
//var es6transpiler = require('gulp-es6-transpiler');
var es6transpiler = require('gulp-babel');

module.export = gulp.task('jses6', function(cb) {
    pathDest = config.pathJsDest || config.pathDest;

    var pathFilenameJavascriptMin = pathDest + config.filenameJavascript + '.min.js';
    var pathFilenameJavascript = pathDest + config.filenameJavascript + '.js';
    gulp.pm.del([pathFilenameJavascriptMin, pathFilenameJavascript],{force:true}, cb);

    var task = gulp.src(config.pathJsES6Src)
        .pipe(gulp.pm.sourcemaps.init())
        .pipe(es6transpiler())

    if (config.minifyJs === true) {
        task.pipe(gulp.pm.uglify())
            .pipe(gulp.pm.rename(config.filenameJavascript + '.min.js'))
    }

    task.pipe(gulp.pm.sourcemaps.write())
        .pipe(gulp.dest(pathDest))
       // .pipe(gulp.pm.livereload());

    var message = gulp.pm.chalk.gray('+-------------------------------+\n')
    message += gulp.pm.chalk.black.bold('Задача jses6 выполнена\n')
    message += gulp.pm.chalk.gray('+-------------------------------+')
    console.info(message);
});