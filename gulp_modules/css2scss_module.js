/**
 * Created by webs on 12.04.15.
 */
var gulp = require('../gulpfile');
var config = gulp.pm.config;
var css2sass = require('gulp-css-scss');

module.export = gulp.task('css2scss', function(cb) {

    pathDest = config.pathScssDest || config.pathDest;

    var pathFilenameStyleMin = pathDest + config.filenameStyle + '.min.css';
    var pathFilenameStyle = pathDest + config.filenameStyle + '.css';

    return gulp.src(pathFilenameStyle)
        .pipe(css2sass())
        .pipe(gulp.dest(pathDest));
});