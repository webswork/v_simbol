/**
 * Created by webs on 12.04.15.
 */
var gulp = require('../gulpfile');
var config = gulp.pm.config;
var sass = require('gulp-sass');

module.export = gulp.task('bower_libs', function(cb) {

    pathDest = config.pathDest;

    var pathFilenameStyleLibsMin = pathDest + config.filenameStyleLibs + '.min.css';
    var pathFilenameStyleLibs = pathDest + config.filenameStyleLibs + '.css';
    var pathFilenameJavascriptLibsMin = pathDest + config.filenameJavascriptLibs + '.min.js';
    var pathFilenameJavascriptLibs = pathDest + config.filenameJavascriptLibs + '.js';
    gulp.pm.del([ pathFilenameStyleLibsMin, pathFilenameStyleLibs, pathFilenameJavascriptLibsMin, pathFilenameJavascriptLibs], {force: true}, cb);

    var task = gulp.src(config.bower.styles)
        .pipe(gulp.pm.sourcemaps.init())
        .pipe(gulp.pm.concatCss(config.filenameStyleLibs + '.css'))
        .pipe(gulp.pm.minifyCSS({keepBreaks:false,keepSpecialComments:0,cache: true}))
        .pipe(gulp.pm.sourcemaps.write())
        .pipe(gulp.dest(pathDest))

    var task = gulp.src(config.bower.javascript)
        .pipe(gulp.pm.sourcemaps.init())
        .pipe(gulp.pm.concatJs(config.filenameJavascriptLibs + '.js'))
        .pipe(gulp.pm.uglify())
        .pipe(gulp.pm.sourcemaps.write())
        .pipe(gulp.dest(pathDest))

    var message = gulp.pm.chalk.gray('+-------------------------------+\n')
    message += gulp.pm.chalk.black.bold('Задача bower_libs выполнена,\nвсе ')
    message += gulp.pm.chalk.cyan.bold('javascript библиотеки\n')
    message += gulp.pm.chalk.black.bold('и ')
    message += gulp.pm.chalk.cyan.bold('библиотеки CSS ')
    message += gulp.pm.chalk.black.bold('перепакованы\n')
    message += gulp.pm.chalk.gray('+-------------------------------+')
    console.info(message);

});