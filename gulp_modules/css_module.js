/**
 * Created by webs on 12.04.15.
 */

var gulp = require('../gulpfile');
var config = gulp.pm.config;
var cssimport = require("gulp-cssimport");


module.export = gulp.task('css', function(cb) {

    pathDest = config.pathCssDest || config.pathDest;

    var pathFilenameCsseMin = pathDest + config.filenameStyle + '.min.css';
    var pathFilenameCss = pathDest + config.filenameStyle + '.css';
    gulp.pm.del([pathFilenameCsseMin, pathFilenameCss],{force:true}, cb);

    var task = gulp.src(config.pathCssSrc)

        .pipe(cssimport( {
            extensions: ["css"] // process only css
        }))
        .pipe(gulp.pm.sourcemaps.init())
        .pipe(gulp.pm.prefix('last 2 version', 'ff', 'op_mini', 'and_chr', 'and_ff', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'));

    if (config.minifyCss === true) {
        task.pipe(gulp.pm.rename(config.filenameStyle + '.min.css'))
            .pipe(gulp.dest(pathDest))
            .pipe(gulp.pm.minifyCSS({keepBreaks:false, keepSpecialComments:0, cache: true}))
            .pipe(gulp.dest(pathDest))
            .pipe(gulp.pm.sourcemaps.write())
            .pipe(gulp.dest(pathDest))
    } else {
        task.pipe(gulp.pm.rename(config.filenameStyle + '.css'))
            .pipe(gulp.dest(config.pathDest))
            .pipe(gulp.pm.sourcemaps.write())
            .pipe(gulp.dest(config.pathDest))
    }

    gulp.src(config.pathDest + '*.css')//.pipe(gulp.pm.livereload());

    var message = gulp.pm.chalk.gray('+-------------------------------+\n')
    message += gulp.pm.chalk.black.bold('Задача css выполнена\n')
    message += gulp.pm.chalk.gray('+-------------------------------+')
    console.info(message);
});