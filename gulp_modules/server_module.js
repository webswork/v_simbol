/**
 * Created by webs on 13.04.15.
 */
var gulp = require('../gulpfile');
var config = gulp.pm.config;

module.export = gulp.task('server', function () {
    'use strict';
  /*  gulp.pm.http
        .createServer(gulp.pm.ecstatic({root: config.pathRoot, showDir: true}))
        .listen(gulp.pm.config.serverport);*/
    // gulp.pm.lrserver.listen(gulp.pm.config.livereloadport);
    // gulp.pm.livereload.listen(gulp.pm.config.livereloadport);

    gulp.pm.browserSync.init({
        logSnippet: false,
        open: false,
        server: {
            baseDir: config.pathRoot
        }
    });

   // var link = 'http://localhost:' + gulp.pm.config.serverport;
  //  var message = gulp.pm.chalk.gray('+--')
   // message += gulp.pm.chalk.black.bold('Server: ' + link)
   // message += gulp.pm.chalk.gray('--+')
   // console.info(message);
    return true;
});
