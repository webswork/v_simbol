/**
 * Created by webs on 13.04.15.
 */
var gulp = require('../gulpfile');
var config = gulp.pm.config;

module.export = gulp.task('html', function (cb) {
    'use strict';

    var pathDest = config.pathHtmlDest || 'public/';

    gulp.src(config.pathHtmlSrc)
        .pipe(gulp.pm.includer())
        .pipe(gulp.dest(pathDest))

    var message = gulp.pm.chalk.gray('+-------------------------------+\n')
    message += gulp.pm.chalk.black.bold('Задача html выполнена\n')
    message += gulp.pm.chalk.gray('+-------------------------------+')
    console.info(message);
});