/**
 * Created by webs on 15.04.15.
 */
var gulp = require('../gulpfile');
var config = gulp.pm.config;

var browserify   = require('browserify');

var autopolyfiller = require('gulp-autopolyfiller');
var order = require('gulp-order');
var merge = require('event-stream').merge;

var vinylSource  = require('vinyl-source-stream');
var vinylBuffer  = require('vinyl-buffer');
var extractor    = require('gulp-extract-sourcemap');
var uglify       = require('gulp-uglifyjs');
var filter       = require('gulp-filter');

module.export = gulp.task('browserify', function (cb) {
    'use strict';
    var pathDest = config.pathJsDest || config.pathDest;
    var pathMapFile = pathDest + config.filenameJavascript + '.js.map';

    var sourcemap = '';
    var exchange = {
        source_map: {
            root: '/',
            orig: ''
        }
    };

    var all = browserify({
        paths:'public/app/js',
        commondir:      false,
        insertGlobals:  true,
        debug: true
    })
        .add(config.mainFile)
        .bundle()
        .pipe(vinylSource(config.filenameJavascript + '.js'))
        .pipe(vinylBuffer())
        .pipe(extractor({
            basedir:pathDest,
            removeSourcesContent: true,
            sourceMappingFileName: config.filenameJavascript + '.js.map'
        }))
        .on('postextract', function (sourceMap) {
            exchange.source_map.orig = sourceMap;
        })
        .pipe(filter('**/*.js'))

    var sourcemap = all;

    var polyfills = all.pipe(autopolyfiller(config.filenameJavascriptPolyfill + '.js', {
        browsers: ['last 2 version', 'ie 9']
    }))
    //.pipe(gulp.pm.uglify());

    var js = merge(polyfills, all)
        .pipe(order([
            config.filenameJavascriptPolyfill + '.js',
            config.filenameJavascript + '.js'
        ]));

    if (config.minifyJs === true) {
        js.pipe(uglify(config.filenameJavascript + '.min.js', {
            outSourceMap: true,
            basePath: pathDest,
            output: {source_map: exchange.source_map}
        })).pipe(gulp.dest(pathDest))
    } else {
        js.pipe(gulp.pm.concatJs(config.filenameJavascript + '.js'))
            .pipe(gulp.dest(pathDest));

    }

    var message = gulp.pm.chalk.gray('+-------------------------------+\n')
    message += gulp.pm.chalk.black.bold('Задача browserify выполнена\n')
    message += gulp.pm.chalk.gray('+-------------------------------+')
    console.info(message);
    return true;
    //return js.pipe(gulp.pm.livereload());
});
/* if (config.minifyJs !== true) {
 var map = JSON.stringify(exchange.source_map.orig);

 fs.open(pathMapFile, 'w', function (err, fd) {
 if (err) throw err;
 fs.write(fd, map, function (err) {if (err) throw err;});
 fs.close(fd, function (err) {if (err) throw err;});
 });
 }*/