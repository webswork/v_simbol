/**
 * Created by webs on 12.04.15.
 */
var gulp = require('../gulpfile');
var config = gulp.pm.config;
var autopolyfiller = require('gulp-autopolyfiller');
var order = require('gulp-order');
var merge = require('event-stream').merge;

module.export = gulp.task('jses5', function(cb) {

    var pathDest = config.pathJsDest || config.pathDest;

    var pathFilenameJavascriptMin = pathDest + config.filenameJavascript + '.min.js';
    var pathFilenameJavascript = pathDest + config.filenameJavascript + '.js';
    gulp.pm.del([pathFilenameJavascriptMin, pathFilenameJavascript],{force:true}, cb);

    var all = gulp.src(config.pathJsSrc)
        .pipe(gulp.pm.concatJs(config.filenameJavascript + '.js'))

    var polyfills = all
        .pipe(autopolyfiller(config.filenameJavascriptPolyfill + '.js', {
            browsers: ['last 2 version', 'ie 8', 'ie 9']
        }))

    var js = merge(polyfills, all)
        // Order files. NB! polyfills MUST be first
        .pipe(order([
            config.filenameJavascriptPolyfill + '.js',
            config.filenameJavascript + '.js'
        ]))
        .pipe( gulp.pm.concatJs(config.filenameJavascript + '.js'))
        .pipe(gulp.pm.sourcemaps.init())

    if (config.minifyJs === true) {
        js.pipe(gulp.pm.uglify())
            .pipe(gulp.pm.rename(config.filenameJavascript + '.min.js'))
    }

    js.pipe(gulp.pm.sourcemaps.write())
    js.pipe(gulp.dest(pathDest))
        //.pipe(gulp.pm.livereload());

    var message = gulp.pm.chalk.gray('+-------------------------------+\n')
    message += gulp.pm.chalk.black.bold('Задача jses5 выполнена\n')
    message += gulp.pm.chalk.gray('+-------------------------------+')
    console.info(message);
});