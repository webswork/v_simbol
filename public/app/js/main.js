/**
 * Created by webs on 12.04.15.
 */
var setting = require('./setting');
require('player');
require('slide_article');
FastClick.attach(document.body);

;(function () {
    'use strict';

    var mainView = Backbone.View.extend({
        el:'body',
        events:{
            'click .js-showmenu': 'showSubmenu',
            'click .js-show_contact_phone': 'showContact',
            'click .js-show_sidebar': 'showSidebar',
            'click .js-hide_sidebar': 'hideSidebar',
            'click .js-show_contact': 'showContactOnHeader',
            'click .js-show_submenu-sidebar': 'showSubmenuSideBar',
            'click .js-show_psy_area_block_schema': 'showPsyareaBlockschema',
            'click .js-show_left_psy_area__cnt__desc': 'showPsyareaBlockschemaLeft',
            'click .js-show_right_psy_area__cnt__desc': 'showPsyareaBlockschemaRight',
            'click .js-prev_reviews': 'slideReviewsPrev',
            'click .js-next_reviews': 'slideReviewsNext',
            'click .js-ask_toggle': 'askToggle',
            'click .js-scroll_up': 'scrollUp',
            'click ': 'closeAll',

            'click .js-open_window': 'openWindow',
            'click .modal .close': 'closeWindow',
           // 'click .shim': 'closeWindow',
            'swiperight .b-mobile_menu_swipe': 'showSidebar',
            'swipeleft .b-nav_panel__sidebar': 'hideSidebar',
            'swiperight .b-cnt_psy_area__cnt__desc': 'showPsyareaBlockschemaRight',
            'swipeleft .b-cnt_psy_area__cnt__desc': 'showPsyareaBlockschemaLeft',
            'swiperight .b-cnt_reviews__cnt__body': 'slideReviewsPrev',
            'swipeleft .b-cnt_reviews__cnt__body': 'slideReviewsNext'

        },
        psy_area_items: [],
        last_el_psy_area_items: '',
        first_el_psy_area_items: 0,
        length_el_psy_area_items: 0,
        el_curr_psy_area_items: 0,
        reviews_items: [],
        initialize: function () {
            var self =  this;

            // плавающая панель
            var header_height = $('.header').height();
            var nav_panel_height = $('.nav_panel').height();
            var full_height = header_height;

            $(window).scroll(function () {
                if (window.pageYOffset >= full_height) {
                    $('.nav_panel').addClass('nav_panel__fixed');
                } else {
                    $('.nav_panel').removeClass('nav_panel__fixed');
                }
            });

            // variable psy_area slider
            var el_psy_area_items =  $('.b-cnt_psy_area__cnt__desc').find('.b-cnt_psy_area__cnt__desc__item');
            self.el_curr_psy_area_items =  $('.b-cnt_psy_area__cnt__desc').find('.b-cnt_psy_area__cnt__desc__item.s-psy_area_desc_item--show').data('id');
            _.each(el_psy_area_items, function (n, m) {
                self.psy_area_items.push($(n).data('id'));
            })
            self.length_el_psy_area_items = self.psy_area_items.length;
            self.last_el_psy_area_items = self.length_el_psy_area_items - 1;

            // yandex map
            var hisMap = $('div').is('#map');
            if (hisMap && _.isObject(ymaps)) {
                ymaps.ready(this.initMap);
            }

       /*     var b = document.documentElement;
            b.setAttribute('data-useragent',  navigator.userAgent);
            b.setAttribute('data-platform', navigator.platform );
            b.className += ((!!('ontouchstart' in window) || !!('onmsgesturechange' in window))?' touch':'');*/

        },
        openWindow: function (e) {
            e.preventDefault();
            var el = $(e.currentTarget).data('window');

            $('#' + el).modal().open();

            $('.shim').css('height',  $(document).height());
            document.addEventListener('orientationchange', function () {
                $('.shim').css('height',  $(document).height());
            }, false);

            /*$('.b-wrapper').hide();
            $('.b-footer').hide();*/
            if (false)
            if (navigator.platform == 'iPad' || navigator.platform == 'iPhone' || navigator.platform == 'iPod') {
                function scroll () {

                    var el_modal = $('.modal');
                    var modal_height = el_modal.height();
                    var fheight = window.innerHeight - modal_height;
                    var partHeight = 0;

                    if (fheight > 0) {
                        partHeight = fheight /// 2;
                    }
                    el_modal.css({
                        position: 'absolute',
                        top: $(document).scrollTop() + partHeight
                    });
                }
                //scroll();
                //document.addEventListener('touchmove', scroll, false);
             }

            var scrollPos = $(document).scrollTop();
            $(window).scroll(function () {
                scrollPos = $(document).scrollTop();
            });
            var savedScrollPos = scrollPos;

            function is_iOS() {
                var iDevices = [
                    'iPad Simulator',
                    'iPhone Simulator',
                    'iPod Simulator',
                    'iPad',
                    'iPhone',
                    'iPod'
                ];
                while (iDevices.length) {
                    if (navigator.platform === iDevices.pop()){ return true; }
                }
                return false;
            }


            $('input[type=text], textarea').on('touchstart', function () {
                if (is_iOS()) {
                    savedScrollPos = scrollPos;
                    $('html').css('overflow', 'hidden');
                }
            }).blur(function () {
                    if (is_iOS()){
                        $(document).scrollTop(savedScrollPos);
                    }
                });
        },
        closeWindow: function (e) {
            e.stopPropagation();
            /*$('.b-wrapper').show();
            $('.b-footer').show();*/
            $.modal().close();
            $('.shim').remove();
            $('body').removeClass('lock');
        },
        initMap:function () {

            var myMap = new ymaps.Map('map', {
                center: [44.0539,43.0622],
                zoom: 15
            });

            myMap.controls.remove('mapTools');
            myMap.controls.remove('typeSelector');
            myMap.controls.remove('zoomControl');
            myMap.controls.remove('routeEditor');
            myMap.controls.remove('searchControl');
            myMap.controls.remove('trafficControl');
            myMap.behaviors.disable('drag')

            var myPlacemark = new ymaps.Placemark([44.0539,43.0622], {
                hintContent: 'Москва!',
                balloonContent: 'Столица России'
            });

            myMap.geoObjects.add(myPlacemark);
        },
        showSubmenu: function (e) {
            e.preventDefault();
            this.$(e.currentTarget).next('.b-nav_panel__line_menu__submenu').toggle();
        },
        hideSubmenu: function (e) {
            e.preventDefault();
            this.$(e.currentTarget + ' .b-nav_panel__line_menu__submenu').hide();
        },
        showOnceSubmenu: function (e) {
            e.preventDefault();
            this.$(e.currentTarget  + ' .b-nav_panel__line_menu__submenu').show();
        },
        showContact: function (e) {

            var hContactList = this.$('.b-contact_popup--position__icon_phone');
            hContactList.toggleClass(
                function () {
                    if (!$(this).is('.contact_popup__icon_phone--animation-bounceInDown')) {
                        hContactList.removeClass('bounceOutUp')
                        return 'contact_popup__icon_phone--animation-bounceInDown';
                    } else {
                        setTimeout(function () {
                            hContactList.removeClass('contact_popup__icon_phone--animation-bounceInDown');
                        }, 600);
                        return 'bounceOutUp';
                    }
                });
        },
        showSidebar: function (e) {

            var npSideBar = $('.b-nav_panel__sidebar');

            /*npSideBar.toggleClass(function () {
                if (!$(this).is('.s-sidebar--show')) {
                    return 's-sidebar--show';
                } else {
                    return 's-sidebar--show';
                }
            });*/
            npSideBar.toggleClass('s-sidebar--show');
        },
        hideSidebar: function (e) {
            var npSideBar = $('.b-nav_panel__sidebar');
            npSideBar.removeClass('s-sidebar--show');
        },
        showContactOnHeader: function (e) {

            var hContactList = this.$('.b-contact_popup');
            hContactList.toggleClass(
                function () {
                    if (!$(this).is('.contact_popup--animation-bounceInDown')) {
                        hContactList.removeClass('bounceOutUp')
                        return 'contact_popup--animation-bounceInDown';
                    } else {
                        setTimeout(function () {
                            hContactList.removeClass('contact_popup--animation-bounceInDown');
                        }, 600);
                        return 'bounceOutUp';
                    }
                });
        },
        showSubmenuSideBar: function (e) {
            e.preventDefault();
            var npItem = this.$(e.currentTarget);
            var npItemParent = npItem.parent('.b-nav_panel__sidebar__menu__item');
            var npItemSubmenu = npItem.next('.b-nav_panel__sidebar__submenu');

            npItemSubmenu.toggle();
            npItemParent.toggleClass(
                function () {
                    if (!$(this).is('.st-nav_panel__sidebar__menu__item--selected')) {
                        return 'st-nav_panel__sidebar__menu__item--selected';
                    } else {
                        return 'st-nav_panel__sidebar__menu__item--selected';
                    }
                });
        },
        showPsyareaBlockschema: function (e) {
            var curr_item =  this.$(e.currentTarget);

            var schema_id =  curr_item.data('id');
            $('.b-cnt_psy_area__cnt__block_schema__item').removeClass('s-psy_area_block_schema_item--selected');
            $('.b-cnt_psy_area__cnt__desc__item').removeClass('s-psy_area_desc_item--show');
            curr_item.addClass('s-psy_area_block_schema_item--selected');
            var g = $('.b-cnt_psy_area__cnt__desc__item' + '[data-id="' + schema_id + '"]').addClass('s-psy_area_desc_item--show');
        },
        showPsyareaBlockschemaLeft:function (e) {
            this._slidePsyArea('left');
        },
        showPsyareaBlockschemaRight:function (e) {
            this._slidePsyArea('right');
        },
        _slidePsyArea: function (swipe) {
            var self = this;
            var selected_element = self.el_curr_psy_area_items;

            if (swipe === 'left') {
                if (selected_element === self.first_el_psy_area_items) {
                    selected_element = self.last_el_psy_area_items;
                } else {
                    selected_element--;
                }

            } else if (swipe === 'right') {

                if (selected_element == self.last_el_psy_area_items) {
                    selected_element = self.first_el_psy_area_items;
                } else {
                    selected_element++;
                }
            }

            var schema_id = self.psy_area_items[selected_element];
            self.el_curr_psy_area_items = selected_element;

            $('.b-cnt_psy_area__cnt__block_schema__item').removeClass('s-psy_area_block_schema_item--selected');
            $('.b-cnt_psy_area__cnt__desc__item').removeClass('s-psy_area_desc_item--show');
            $('.b-cnt_psy_area__cnt__desc__item' + '[data-id="' + schema_id + '"]')
                .addClass('s-psy_area_desc_item--show');

            $('.b-cnt_psy_area__cnt__block_schema__item' + '[data-id="' + schema_id + '"]')
                .addClass('s-psy_area_block_schema_item--selected');
        },
        slideReviewsPrev: function (e) {
            this._slideReviews('prev');
        },
        slideReviewsNext: function (e) {
            this._slideReviews('next');
        },
        _slideReviews: function (slide) {

            var self = this;

            var el_curr_reviews_items =  $('.b-cnt_reviews')
                .find('.b-cnt_reviews__cnt__item.s-cnt_reviews_item--selected')
                .data('id');

            var el_reviews_items =  $('.b-cnt_reviews').find('.b-cnt_reviews__cnt__item');
            self.reviews_items = [];
            _.each(el_reviews_items, function (n, m) {
                self.reviews_items.push($(n).data('id'));
            })

            var length_reviws = self.reviews_items.length;
            var el_index_last = length_reviws -1;
            var el_index_first = 0;
            el_curr_reviews_items--;

            if (slide === 'prev') {
                if (el_curr_reviews_items == el_index_first) {
                    el_curr_reviews_items = el_index_last;
                } else {
                    el_curr_reviews_items--;
                }
            } else if (slide === 'next') {
                if (el_curr_reviews_items == el_index_last){
                    el_curr_reviews_items = el_index_first;
                } else {
                    el_curr_reviews_items++;
                }
            }

            var index = self.reviews_items[el_curr_reviews_items];

            $('.b-cnt_reviews__cnt__item').removeClass('s-cnt_reviews_item--selected');
            $('.b-cnt_reviews__cnt__item').removeClass('a-cnt_reviews_fade_left');
            $('.b-cnt_reviews__cnt__item').removeClass('a-cnt_reviews_fade_right');

            var el_selected = $('.b-cnt_reviews__cnt__item' + '[data-id="' + index + '"]');

            if (slide === 'next') el_selected.addClass('s-cnt_reviews_item--selected a-cnt_reviews_fade_right');
            if (slide === 'prev')el_selected.addClass('s-cnt_reviews_item--selected a-cnt_reviews_fade_left');
        },
        askToggle: function (e) {
            var el_curr = this.$(e.currentTarget);

            el_curr.toggleClass(
                function () {
                    var el_ask__icon = $(this).children('.b-cnt_ask__icon');
                    var el_ask_items__text = $(this).next('.b-cnt_ask__cnt__items__text');


                    if (!$(this).is('.s-cnt_ask__cnt__items__title--selected')) {
                        el_ask__icon.html('&#xe842;');
                        el_ask_items__text.addClass('s-cnt_ask__cnt__items__text--show');
                        return 's-cnt_ask__cnt__items__title--selected';
                    } else {
                        el_ask__icon.html('&#xe843;');
                        el_ask_items__text.removeClass('s-cnt_ask__cnt__items__text--show');
                        return 's-cnt_ask__cnt__items__title--selected';
                    }
                });
        },
        scrollUp: function () {
            $('html, body').animate({scrollTop: 0}, 'slow');
            return false;
        },
        closeAll: function (e) {
            //e.preventDefault();

            var hContactList = $('.b-contact_popup');
            var npLinemenuSubmenu =  $('.b-nav_panel__line_menu__submenu');
            var npMobilemenuContactList =  $('.b-contact_popup--position__icon_phone');
            var npSideBar = $('.b-nav_panel__sidebar');

            if (!$(e.target).hasClass('js-show_contact')) {
                hContactList.addClass('bounceOutUp');
                setTimeout(function () {
                    hContactList.removeClass('contact_popup--animation-bounceInDown');
                }, 600);
            }
            if (!$(e.target).hasClass('js-show_contact_phone')) {
                npMobilemenuContactList.addClass('bounceOutUp');
                setTimeout(function () {
                    npMobilemenuContactList.removeClass('contact_popup__icon_phone--animation-bounceInDown');
                }, 600);
            }

            if (!$(e.target).hasClass('js-show_sidebar') && !$(e.target).hasClass('js-show_submenu-sidebar')) {
                npSideBar.removeClass('s-sidebar--show');
            }

            if (!$(e.target).hasClass('js-showmenu')) {
                npLinemenuSubmenu.hide();
            }

        }
    });
    var view_main = new mainView();

    var serviceModalStudyAdvice = Backbone.View.extend({
        el: 'body',
        events: {
            'change .js-study_advice_meet_radio': 'studyAdviceMeet',
            'click #study_advice_mail': 'validateMail',
            'keyup #study_advice_mail': 'validateMail',
            'change #study_advice_mail': 'validateMail',
            'keypress #study_advice_phone': 'inputPhone',
            'keypress #study_advice_name': 'inputName',
            'click .js-study_advice_submit': 'sendData',
            'keyup .b-cnt_contact__cnt__form_study_advice__form': 'validateErr',
            'change .b-cnt_contact__cnt__form_study_advice__form': 'validateErr',

            'keyup #study_advice': 'validateAll',
            'change #study_advice': 'validateErr',
        },
        advice_meet_radio: false,
        email_validated: false,
        phone_validated: false,
        initialize: function () {
            var self = this;
            $('#study_advice').bind('change keyup blur input', function (e) {
                self.validateErr(e)
                self.validateAll(e)
            });
            $('#study_advice_mail').bind('change keyup blur input', function (e) {
                self.validateMail(e)

            });

        },
        sendData: function (e) {
            e.preventDefault();

            $('.modal').css('height', '107px');
            $('.modal_cnt').css('margin-bottom', '0px');
            $('.modal_cnt__subtitlemain').hide();
            $('.b-cnt_contact__cnt__form_study_advice__form').hide();
            $('.js_send_messages_complite').removeClass('hide');
            $('.js_send_messages_complite').show();

            _.delay(function () {
                /*$('.b-wrapper').show();
                $('.b-footer').show();*/
                $.modal().close();
                $('.modal_cnt__subtitlemain').show();
                $('.b-cnt_contact__cnt__form_study_advice__form').show();
                $('.js_send_messages_complite').addClass('hide');
                $('.b-cnt_contact__cnt__form_study_advice__submit').attr('disabled', true);
                $('.b-study_advice_skype').hide();
                $('.b-study_advice_meet_address').show();
                $('.modal').css('height', 'auto');
            }, 3000);
        },
        studyAdviceMeet: function (e) {
            var el_radio = $(e.currentTarget);
            var el_b_skype = $('.b-study_advice_skype');
            var el_b_meet_address = $('.b-study_advice_meet_address');

            el_b_skype.hide();
            el_b_meet_address.show();
            this.advice_meet_radio = false;
            if (el_radio.val() == 2) {
                el_b_skype.show();
                el_b_meet_address.hide();
                this.advice_meet_radio = true;
            }
            this.validateAll(e);
        },
        inputName:function (e) {
            return (e.which != 44 && e.which != 33
                && e.which != 64 && e.which != 35 && e.which != 36 && e.which != 37 && e.which != 94 && e.which != 38
                && e.which != 42 && e.which != 40 && e.which != 41 && e.which != 95 && e.which != 43 && e.which != 61
                && e.which != 45 && e.which != 126 && e.which != 96 && e.which != 123 && e.which != 125 && e.which != 91
                && e.which != 93 && e.which != 92 && e.which != 124 && e.which != 63 && e.which != 47 && e.which != 62
                && e.which != 60 && e.which != 43 && e.which != 45 &&  e.which != 40 && e.which != 41
                &&  e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)
            ) ? true : false  ;
        },
        inputPhone:function (e) {
            return (e.which != 44 && e.which != 43 && e.which != 45 && e.which != 32 && e.which != 40 && e.which != 41
                &&  e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)
            ) ? false : true ;
        },
        validateMail: function (e) {
            var el_mail = $(e.currentTarget);
            var email = el_mail.val();
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if (pattern.test(email)){
                this.email_validated = true;
                el_mail.removeClass('s-form-error');
            } else {
                this.email_validated = false;
                el_mail.addClass('s-form-error');
            }
        },
        validateAll: function (e) {
            e.preventDefault();
            var data = Backbone.Syphon.serialize(this);

            var size_valid_field = _.size(data.study_advice);
            if (this.advice_meet_radio === false) {
                delete data.study_advice.skype;
                size_valid_field = _.size(data.study_advice);
            }

            var count_valid_filed = 0;
            _.each(data.study_advice, function (v, k) {
                if (!_.isEmpty(v)) {
                    count_valid_filed++;
                }
            });

            var el_submit = $('.b-cnt_contact__cnt__form_study_advice__submit');
            if (count_valid_filed === size_valid_field && this.email_validated === true && this.phone_validated === true) {
                el_submit.attr('disabled', false);
            } else {
                el_submit.attr('disabled', true);
            }
        },
        validateErr: function (e) {
            e.preventDefault();
            var data = Backbone.Syphon.serialize(this);
            var self = this;
            _.each(data.study_advice, function (v, k) {

                if (k === 'phone') {
                    if (_.size(v) >= 5) {
                        $('#study_advice_' + k).removeClass('s-form-error');
                        self.phone_validated = true;
                    } else {
                        $('#study_advice_' + k).addClass('s-form-error');
                        self.phone_validated = false;
                    }
                } else {
                    if (!_.isEmpty(v)) {
                        if (k == 'mail') {} else {
                            $('#study_advice_' + k).removeClass('s-form-error');
                        }
                    } else {
                        $('#study_advice_' + k).addClass('s-form-error');
                    }
                }
            });
        }
    });
    var service_modal_study_advice = new serviceModalStudyAdvice();

    var serviceModalSkypeConsultancy = Backbone.View.extend({
        el: 'body',
        events: {

            'keyup .b-cnt_contact__cnt__form_study_advice__form': 'validateErr',
            'change .b-cnt_contact__cnt__form_study_advice__form': 'validateErr',
            'keyup #skype_consultancy_mail': 'validateMail',
            'change #skype_consultancy_mail': 'validateMail',
            'keypress #skype_consultancy_phone': 'inputPhone',
            'keypress #skype_consultancy_name': 'inputName',
            'click .js-skype_consultancy_submit': 'sendData',
            'keyup #skype_consultancy': 'validateAll',
            'change #skype_consultancy': 'validateAll',
        },
        email_validated: false,
        phone_validated: false,
        initialize: function () {
            var self = this;
            $('#skype_consultancy').bind('change keyup blur input', function (e) {
                self.validateErr(e)
                self.validateAll(e)
            });
            $('#skype_consultancy_mail').bind('change keyup blur input', function (e) {
                self.validateMail(e)

            });
        },
        sendData: function (e) {
            e.preventDefault();

            $('.modal').css('height', '107px');
            $('.modal_cnt').css('margin-bottom', '0px');
            $('.modal_cnt__subtitlemain').hide();
            $('.b-cnt_contact__cnt__form_study_advice__form').hide();
            $('.js_send_messages_complite').removeClass('hide');
            $('.js_send_messages_complite').show();

            _.delay(function () {
                /*$('.b-wrapper').show();
                $('.b-footer').show();*/
                $.modal().close();
                $('.modal_cnt__subtitlemain').show();
                $('.b-cnt_contact__cnt__form_study_advice__form').show();
                $('.js_send_messages_complite').addClass('hide');
                $('.b-cnt_contact__cnt__form_study_advice__submit').attr('disabled', true);
                $('.modal').css('height', 'auto');
            }, 3000);
        },
        inputName:function (e) {
            return (e.which != 44 && e.which != 33
                && e.which != 64 && e.which != 35 && e.which != 36 && e.which != 37 && e.which != 94 && e.which != 38
                && e.which != 42 && e.which != 40 && e.which != 41 && e.which != 95 && e.which != 43 && e.which != 61
                && e.which != 45 && e.which != 126 && e.which != 96 && e.which != 123 && e.which != 125 && e.which != 91
                && e.which != 93 && e.which != 92 && e.which != 124 && e.which != 63 && e.which != 47 && e.which != 62
                && e.which != 60 && e.which != 43 && e.which != 45  && e.which != 40 && e.which != 41
                &&  e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)
            ) ? true : false  ;
        },
        inputPhone:function (e) {
            return (e.which != 44 && e.which != 43 && e.which != 45 && e.which != 32 && e.which != 40 && e.which != 41
                &&  e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)
            ) ? false : true ;
        },
        validateMail: function (e) {
            var el_mail = $(e.currentTarget);
            console.info(el_mail);
            var email = el_mail.val();
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if (pattern.test(email)){
                this.email_validated = true;
                el_mail.removeClass('s-form-error');
            } else {
                this.email_validated = false;
                el_mail.addClass('s-form-error');
            }
        },
        validateAll: function (e) {
            e.preventDefault();
            var data = Backbone.Syphon.serialize(this);

            var size_valid_field = _.size(data.skype_consultancy);

            var count_valid_filed = 0;
            _.each(data.skype_consultancy, function (v, k) {
                if (!_.isEmpty(v)) {
                    if (k === 'phone' && _.size(v) >= 5) {
                        count_valid_filed++;
                    } else if (k !== 'phone') {
                        count_valid_filed++;
                    }
                }
            });

            var el_submit = $('.b-cnt_contact__cnt__form_study_advice__submit');
            if (count_valid_filed === size_valid_field && this.email_validated === true && this.phone_validated === true) {
                el_submit.attr('disabled', false);
            } else {
                el_submit.attr('disabled', true);
            }
        },
        validateErr: function (e) {
            e.preventDefault();
            var data = Backbone.Syphon.serialize(this);
            var self = this;
            _.each(data.skype_consultancy, function (v, k) {

                if (k === 'phone') {
                    if (_.size(v) >= 5) {
                        $('#skype_consultancy_' + k).removeClass('s-form-error');
                        self.phone_validated = true;
                    } else {
                        $('#skype_consultancy_' + k).addClass('s-form-error');
                        self.phone_validated = false;
                    }
                } else {
                    if (!_.isEmpty(v)) {
                        if (k == 'mail') {} else {
                            $('#skype_consultancy_' + k).removeClass('s-form-error');
                        }
                    } else {
                        $('#skype_consultancy_' + k).addClass('s-form-error');
                    }
                }
            });
        }
    });
    var service_modal_study_advice = new serviceModalSkypeConsultancy();

    var serviceModalIndividualCounselingy = Backbone.View.extend({
        el: 'body',
        events: {

            'keyup .b-cnt_contact__cnt__form_study_advice__form': 'validateErr',
            'change .b-cnt_contact__cnt__form_study_advice__form': 'validateErr',
            'keyup #individual_counseling_mail': 'validateMail',
            'change #individual_counseling_mail': 'validateMail',
            'keypress #individual_counseling_phone': 'inputPhone',
            'keypress #individual_counseling_name': 'inputName',
            'click .js-individual_counseling_submit': 'sendData',
            'keyup #individual_counseling': 'validateAll',
            'change #individual_counseling': 'validateAll',
        },
        email_validated: false,
        phone_validated: false,
        initialize: function () {
            var self = this;
            $('#individual_counseling').bind('change keyup blur input', function (e) {
                self.validateErr(e)
                self.validateAll(e)
            });
            $('#individual_counseling_mail').bind('change keyup blur input', function (e) {
                self.validateMail(e)

            });
        },
        sendData: function (e) {
            e.preventDefault();

            $('.modal').css('height', '107px');
            $('.modal_cnt').css('margin-bottom', '0px');
            $('.modal_cnt__subtitlemain').hide();
            $('.b-cnt_contact__cnt__form_study_advice__form').hide();
            $('.js_send_messages_complite').removeClass('hide');
            $('.js_send_messages_complite').show();

            _.delay(function () {
                /*$('.b-wrapper').show();
                $('.b-footer').show();*/
                $.modal().close();
                $('.modal_cnt__subtitlemain').show();
                $('.b-cnt_contact__cnt__form_study_advice__form').show();
                $('.js_send_messages_complite').addClass('hide');
                $('.b-cnt_contact__cnt__form_study_advice__submit').attr('disabled', true);
                $('.modal').css('height', 'auto');
            }, 3000);
        },
        inputName:function (e) {
            return (e.which != 44 && e.which != 33
                && e.which != 64 && e.which != 35 && e.which != 36 && e.which != 37 && e.which != 94 && e.which != 38
                && e.which != 42 && e.which != 40 && e.which != 41 && e.which != 95 && e.which != 43 && e.which != 61
                && e.which != 45 && e.which != 126 && e.which != 96 && e.which != 123 && e.which != 125 && e.which != 91
                && e.which != 93 && e.which != 92 && e.which != 124 && e.which != 63 && e.which != 47 && e.which != 62
                && e.which != 60 && e.which != 43 && e.which != 45  && e.which != 40 && e.which != 41
                &&  e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)
            ) ? true : false  ;
        },
        inputPhone:function (e) {
            return (e.which != 44 && e.which != 43 && e.which != 45 && e.which != 32 && e.which != 40 && e.which != 41
                &&  e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)
            ) ? false : true ;
        },
        validateMail: function (e) {
            var el_mail = $(e.currentTarget);

            var email = el_mail.val();
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if (pattern.test(email)){
                this.email_validated = true;
                el_mail.removeClass('s-form-error');
            } else {
                this.email_validated = false;
                el_mail.addClass('s-form-error');
            }
        },
        validateAll: function (e) {
            e.preventDefault();
            var data = Backbone.Syphon.serialize(this);

            var size_valid_field = _.size(data.individual_counseling);

            var count_valid_filed = 0;
            _.each(data.individual_counseling, function (v, k) {
                if (!_.isEmpty(v)) {
                    if (k === 'phone' && _.size(v) >= 5) {
                        count_valid_filed++;
                    } else if (k !== 'phone') {
                        count_valid_filed++;
                    }
                }
            });

            var el_submit = $('.b-cnt_contact__cnt__form_study_advice__submit');
            if (count_valid_filed === size_valid_field && this.email_validated === true && this.phone_validated === true) {
                el_submit.attr('disabled', false);
            } else {
                el_submit.attr('disabled', true);
            }
        },
        validateErr: function (e) {
            e.preventDefault();
            var data = Backbone.Syphon.serialize(this);
            var self = this;
            _.each(data.individual_counseling, function (v, k) {

                if (k === 'phone') {
                    if (_.size(v) >= 5) {
                        $('#individual_counseling_' + k).removeClass('s-form-error');
                        self.phone_validated = true;
                    } else {
                        $('#individual_counseling_' + k).addClass('s-form-error');
                        self.phone_validated = false;
                    }
                } else {
                    if (!_.isEmpty(v)) {
                        if (k == 'mail') {} else {
                            $('#individual_counseling_' + k).removeClass('s-form-error');
                        }
                    } else {
                        $('#individual_counseling_' + k).addClass('s-form-error');
                    }
                }
            });
        }
    });
    var service_modal_individual_counseling = new serviceModalIndividualCounselingy();

    var serviceModalAppointment = Backbone.View.extend({
        el: 'body',
        events: {
            'keyup .b-cnt_contact__cnt__form_appointment__form': 'validateErr',
            'change .b-cnt_contact__cnt__form_appointment__form': 'validateErr',
            'keypress #appointment_phone': 'inputPhone',
            'keypress #appointment_name': 'inputName',
            'click .js-appointment_submit': 'sendData',
            'keyup #appointment': 'validateAll',
            'change #appointment': 'validateAll',
        },
        phone_validated: false,
        initialize: function () {
            var self = this;
            $('#appointment').bind('change keyup blur input', function (e) {
                self.validateErr(e)
                self.validateAll(e)
            });
        },
        sendData: function (e) {
            e.preventDefault();

            $('.modal').css('height', '107px');
            $('.modal_cnt').css('margin-bottom', '0px');
            $('.modal_cnt__subtitlemain').hide();
            $('.b-cnt_contact__cnt__form_appointment__form').hide();
            $('.js_send_messages_complite').removeClass('hide');
            $('.js_send_messages_complite').show();

            _.delay(function () {
                /*$('.b-wrapper').show();
                $('.b-footer').show();*/
                $.modal().close();
                $('.modal_cnt__subtitlemain').show();
                $('.b-cnt_contact__cnt__form_appointment__form').show();
                $('.js_send_messages_complite').addClass('hide');
                $('.js-appointment_submit').attr('disabled', true);
                $('.modal').css('height', 'auto');
            }, 3000);
        },
        inputName:function (e) {
            return (e.which != 44 && e.which != 33
                && e.which != 64 && e.which != 35 && e.which != 36 && e.which != 37 && e.which != 94 && e.which != 38
                && e.which != 42 && e.which != 40 && e.which != 41 && e.which != 95 && e.which != 43 && e.which != 61
                && e.which != 45 && e.which != 126 && e.which != 96 && e.which != 123 && e.which != 125 && e.which != 91
                && e.which != 93 && e.which != 92 && e.which != 124 && e.which != 63 && e.which != 47 && e.which != 62
                && e.which != 60 && e.which != 43 && e.which != 45  && e.which != 40 && e.which != 41
                &&  e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)
            ) ? true : false  ;
        },
        inputPhone:function (e) {
            return (e.which != 44 && e.which != 43 && e.which != 45 && e.which != 32 && e.which != 40 && e.which != 41
                &&  e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)
            ) ? false : true ;
        },

        validateAll: function (e) {
            e.preventDefault();
            var data = Backbone.Syphon.serialize(this);

            var size_valid_field = _.size(data.appointment);

            var count_valid_filed = 0;
            _.each(data.appointment, function (v, k) {
                if (!_.isEmpty(v)) {
                    if (k === 'phone' && _.size(v) >= 5) {
                        count_valid_filed++;
                    } else if (k !== 'phone') {
                        count_valid_filed++;
                    }
                }
            });

            var el_submit = $('.js-appointment_submit');
            if (count_valid_filed === size_valid_field && this.phone_validated === true) {
                el_submit.attr('disabled', false);
            } else {
                el_submit.attr('disabled', true);
            }
        },
        validateErr: function (e) {
            e.preventDefault();
            var data = Backbone.Syphon.serialize(this);
            var self = this;
            _.each(data.appointment, function (v, k) {

                if (k === 'phone') {
                    if (_.size(v) >= 5) {
                        $('#appointment_' + k).removeClass('s-form-error');
                        self.phone_validated = true;
                    } else {
                        $('#appointment_' + k).addClass('s-form-error');
                        self.phone_validated = false;
                    }
                } else {
                    if (!_.isEmpty(v)) {

                    } else {
                        $('#appointment_' + k).addClass('s-form-error');
                    }
                }
            });
        }
    });
    var service_modal_appointment = new serviceModalAppointment();

    var serviceModalYourQuestion = Backbone.View.extend({
        el: 'body',
        events: {

            'keyup .b-cnt_contact__cnt__form_study_advice__form': 'validateErr',
            'change .b-cnt_contact__cnt__form_study_advice__form': 'validateErr',
            'keyup #your_question_mail': 'validateMail',
            'change #your_question_mail': 'validateMail',
            'keypress #your_question_phone': 'inputPhone',
            'keypress #your_question_name': 'inputName',
            'click .js-your_question_submit': 'sendData',
            'keyup #your_question': 'validateAll',
            'change #your_question': 'validateAll',
        },
        email_validated: false,
        phone_validated: false,
        initialize: function () {
            var self = this;
            $('#your_question').bind('change keyup blur input', function (e) {
                self.validateErr(e)
                self.validateAll(e)
            });
            $('#your_question_mail').bind('change keyup blur input', function (e) {
                self.validateMail(e)

            });
        },
        sendData: function (e) {
            e.preventDefault();

            $('.modal').css('height', '107px');
            $('.modal_cnt').css('margin-bottom', '0px');
            $('.modal_cnt__subtitlemain').hide();
            $('.b-cnt_contact__cnt__form_study_advice__form').hide();
            $('.js_send_messages_complite').removeClass('hide');
            $('.js_send_messages_complite').show();

            _.delay(function () {
                /*$('.b-wrapper').show();
                $('.b-footer').show();*/
                $.modal().close();
                $('.modal_cnt__subtitlemain').show();
                $('.b-cnt_contact__cnt__form_study_advice__form').show();
                $('.js_send_messages_complite').addClass('hide');
                $('.js-your_question_submit').attr('disabled', true);
                $('.modal').css('height', 'auto');
            }, 3000);
        },
        inputName:function (e) {
            return (e.which != 44 && e.which != 33
                && e.which != 64 && e.which != 35 && e.which != 36 && e.which != 37 && e.which != 94 && e.which != 38
                && e.which != 42 && e.which != 40 && e.which != 41 && e.which != 95 && e.which != 43 && e.which != 61
                && e.which != 45 && e.which != 126 && e.which != 96 && e.which != 123 && e.which != 125 && e.which != 91
                && e.which != 93 && e.which != 92 && e.which != 124 && e.which != 63 && e.which != 47 && e.which != 62
                && e.which != 60 && e.which != 43 && e.which != 45  && e.which != 40 && e.which != 41
                &&  e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)
            ) ? true : false  ;
        },
        inputPhone:function (e) {
            return (e.which != 44 && e.which != 43 && e.which != 45 && e.which != 32 && e.which != 40 && e.which != 41
                &&  e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)
            ) ? false : true ;
        },
        validateMail: function (e) {
            var el_mail = $(e.currentTarget);

            var email = el_mail.val();
            var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,4}\.)?[a-z]{2,4}$/i;
            if (pattern.test(email)){
                this.email_validated = true;
                el_mail.removeClass('s-form-error');
            } else {
                this.email_validated = false;
                el_mail.addClass('s-form-error');
            }
        },
        validateAll: function (e) {
            e.preventDefault();
            var data = Backbone.Syphon.serialize(this);

            var size_valid_field = _.size(data.your_question);

            var count_valid_filed = 0;
            _.each(data.your_question, function (v, k) {
                if (!_.isEmpty(v)) {
                   /* if (k === 'phone' && _.size(v) >= 5) {
                        count_valid_filed++;
                    } else*/ if (k !== 'phone') {
                        count_valid_filed++;
                    }
                }
            });

            var el_submit = $('.js-your_question_submit');
            if (count_valid_filed === size_valid_field && this.email_validated === true /*&& this.phone_validated === true*/) {
                el_submit.attr('disabled', false);
            } else {
                el_submit.attr('disabled', true);
            }
        },
        validateErr: function (e) {
            e.preventDefault();
            var data = Backbone.Syphon.serialize(this);

            var self = this;
            _.each(data.your_question, function (v, k) {

                if (k === 'phone') {
                    if (_.size(v) >= 5) {
                        $('#your_question_' + k).removeClass('s-form-error');
                        self.phone_validated = true;
                    } else {
                        $('#your_question_' + k).addClass('s-form-error');
                        self.phone_validated = false;
                    }
                } else {
                    if (!_.isEmpty(v)) {
                        if (k == 'mail') {} else {
                            $('#your_question_' + k).removeClass('s-form-error');
                        }
                    } else {
                        $('#your_question_' + k).addClass('s-form-error');
                    }
                }
            });
        }
    });
    var service_modal_your_question = new serviceModalYourQuestion();
})();