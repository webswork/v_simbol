/**
 * Created by webs on 31.10.15.
 */
;(function () {
    'use strict';
    var counter = 0;
    var slideView = Backbone.View.extend({
        el: 'body',
        events: {
            'click .js-prev_slide': 'slideLeft',
            'click .js-next_slide': 'slideRight',
            'swiperight .b-slide_article': 'slideLeft',
            'swipeleft .b-slide_article': 'slideRight',
        },
        status_item_show: '.s-slide_article__item--show',
        el_slide: $('.b-slide_article__cnt'),

        memo: memo(),
        initialize: function () {
            this.slideWidthChange();
        },
        slideWidthChange: function (slide_width) {
            var items = this.el_slide.children('.b-slide_article__item');
            counter = 0;

            items.removeClass('s-slide_article__item--show');
            items.removeAttr('style');
            _.each(items, function (v, k) {
                if ($(v).css('visibility') == 'visible') {
                    $(v).addClass('s-slide_article__item--show');
                    counter++;
                } else {

                    $(v).removeClass('s-slide_article__item--show');
                }
            });
        },
        slideLeft: function (e) {
            this._slide('left');
        },
        slideRight: function (e) {
            this._slide('right');
        },
        _slide: function (go) {
            var items = this.el_slide.children('.b-slide_article__item');
            var next = [];

            _.each(items, function (v,k) {
                if ($(v).hasClass('s-slide_article__item--show')) {
                    if (k >= 0) {
                        next.push(k);
                    }
                }
            });
            this.memo(next);

            this._leftRightStep(go, counter);
            this.show(go);
        },
        _leftRightStep: function (go, count_items) {
            var last_i = [];
            var cur_i = '';
            var count_items = count_items || 1 ;
            var next = [];
            var items_len = this.el_slide.children('.b-slide_article__item').length;
            items_len--;

            if (go === 'left') {
                last_i = _.take(this.memo(), 1).pop();
                if (!_.isUndefined(last_i)) {
                    cur_i = last_i
                    cur_i = last_i - 1;

                    if (cur_i >= 0) {
                        var m = 1;
                        for (var i = cur_i; i >= 0; i--) {
                            if (m <= count_items){
                                m++;
                                next.push(i);
                            }
                        }
                    }
                    this.memo(next);
                }

            } else if (go === 'right') {
                last_i = _.takeRight(this.memo(), 1).pop();
                if (!_.isUndefined(last_i)) {
                    cur_i = last_i
                    cur_i = last_i + 1;

                    if (cur_i <= items_len) {
                        var m = 1;
                        for (var i = cur_i; i <= items_len; i++) {
                            if (m <= count_items){
                                m++;
                                next.push(i);
                            }
                        }
                    }
                    this.memo(next);
                }
            }
        },
        show: function (go) {

            // fx
            var class_fx = '';
            if (go === 'left') {
                class_fx = 'fadeInLeft'
            } else if (go === 'right') {
                class_fx = 'fadeInRight'
            }

            var selected_items = this.memo();
            if (!_.isEmpty(selected_items)) {
                var items = this.el_slide.children('.b-slide_article__item');

                items.removeClass('s-slide_article__item--show')
                items.removeClass('fadeInLeft')
                items.removeClass('fadeInRight')
                items.hide();
                _.each(items, function (v,k) {
                    $(items[selected_items[k]]).addClass('s-slide_article__item--show');
                    $(items[selected_items[k]]).addClass(class_fx);
                    $(items[selected_items[k]]).show();

                });
            }

        }
    });

    var view_slide = new slideView();

    var mql = window.matchMedia('(orientation: portrait)');
    if (mql.matches) {
        view_slide.slideWidthChange();
    } else {
        view_slide.slideWidthChange();
    }
    mql.addListener(function (m) {
        if (m.matches) {
            view_slide.slideWidthChange();
        } else {
            view_slide.slideWidthChange();
        }
    });
    function memo() {
        var m = [];
        return function (n) {

            if (!n) {
                return m
            } else {
                m = n;
            }
        }
    }

})();

