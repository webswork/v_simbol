/**
 * Created by webs on 28.10.15.
 */
;(function () {
    'use strict';
    var _player = {};

    function preloadImages(srcArray) {
        for (var i = 0, len = srcArray.length; i < len; i++) {
            var img = new Image();
            img.src = srcArray[i];
            img.style.display = 'none';
            document.body.appendChild(img);
        }
    }

    preloadImages([
        '../assets/player/btn_pause.png',
        '../assets/player/btn_play.png',
        '../assets/player/btn_volume_up_press.png',
        '../assets/player/btn_volume_up.png',
        '../assets/player/btn_volume_down_press.png',
        '../assets/player/btn_volume_down.png',
        '../assets/player/btn_volume_mute.png',
        '../assets/player/togo_next_selected.png',
        '../assets/player/togo_next.png',
        '../assets/player/togo_prev_selected.png',
        '../assets/player/togo_prev.png',
    ])

    var playerView = Backbone.View.extend({
        el: '.player',
        events:{
            'click .togonext': 'togoNext',
            'click .togoback': 'togoBack',
            'click .volume_mute': 'volumeMute',
            'click .volume_p': 'volumeUp',
            'click .volume_m': 'volumeDown'
        },
        player_mute_set: [],
        player_volume_set: [],
        initialize: function () {

            audiojs.events.ready(function () {
                _player = audiojs.createAll();
                var tmpl_controll = '<div class="controll"><div class="togoback"><div class="togobackw"></div></div><div class="togonext"><div class="togonextw"></div></div>' +
                    '<div class="volume_mute"></div><div class="volume_m"><div class="volume_m_p"></div></div><div class="volume_p"><div class="volume_p_p"></div></div>'
                '</div>';
                var tmpl_status_file = '<div class="player_filestatus"><a  type="audio/mpeg3"  class="player_filestatus__link" download >Скачать в mp3</a><div class="player_filestatus__status">(<span class="player_filestatus__status_size"></span> / <span class="player_filestatus__status_duration"></span>)</div></div>';
                $('.audiojs').append(tmpl_controll);
                $('.audiojs').append(tmpl_status_file);
                $('.audiojs .progress').html('<div class="pimp"></div>');

            });
        },
        volumeMute: function (e) {
            var curr = this._getData(e);
            if (curr.player_key) {

                var audio = _player[curr.player_key];

                if (this.player_mute_set[curr.player_key] == 'mute') {
                    this.player_mute_set[curr.player_key] = 'hi';
                    audio.setVolume(30 / 100);
                } else {
                    this.player_mute_set[curr.player_key] = 'mute';
                    audio.setVolume(0);
                }
            }
        },
        volumeUp: function (e) {
            var curr = this._getData(e);
            curr.el.children().fadeTo(0, 1).delay(250).fadeTo(0, 0)
            if (curr.player_key) {
                var audio = _player[curr.player_key];

                if (!this.player_volume_set[curr.player_key]) {
                    this.player_volume_set[curr.player_key] = audio.element.volume;
                }

                var inc = this.player_volume_set[curr.player_key];
                if (inc <= 1) {
                    inc += 0.1;
                    if (inc > 1) inc = 1;
                    this.player_volume_set[curr.player_key] = inc;
                    audio.setVolume(this.player_volume_set[curr.player_key]);
                } else {
                    this.player_volume_set[curr.player_key] = 1;
                    audio.setVolume(this.player_volume_set[curr.player_key]);
                }
            }
        },
        volumeDown: function (e) {
            var curr = this._getData(e);
            curr.el.children().fadeTo(0, 1).delay(250).fadeTo(0, 0);

            if (curr.player_key) {
                var audio = _player[curr.player_key];

                if (!this.player_volume_set[curr.player_key]) {
                    this.player_volume_set[curr.player_key] = audio.element.volume;
                }

                var inc = this.player_volume_set[curr.player_key];
                if (inc <= 1 && inc > 0) {
                    inc -= 0.1;
                    if (inc < 0) inc = 0;
                    this.player_volume_set[curr.player_key] = inc;
                    audio.setVolume(this.player_volume_set[curr.player_key]);
                } else {
                    this.player_volume_set[curr.player_key] = 0;
                    audio.setVolume(this.player_volume_set[curr.player_key]);
                }
            }
        },
        togoNext: function (e) {
            var curr = this._getData(e);
            curr.el.children().fadeTo(0, 1).delay(250).fadeTo(0, 0)

            if (curr.player_key) {
                var audio = _player[curr.player_key];
                var curr_position = audio.element.currentTime / audio.element.duration;

                if (curr_position !== 0) {
                    curr_position += 0.1;
                    audio.skipTo(curr_position);
                }
            }
        },

        togoBack: function (e) {
            var curr = this._getData(e);
            curr.el.children().fadeTo(0, 1).delay(250).fadeTo(0, 0)

            if (curr.player_key) {
                var audio = _player[curr.player_key];
                var curr_position = audio.element.currentTime / audio.element.duration;

                if (curr_position !== 0) {
                    curr_position -= 0.1;
                    audio.skipTo(curr_position);
                }
            }
        },
        _getData: function (eventer) {
            var el_curr = this.$(eventer.currentTarget);
            var player_id = el_curr.parent().parent().attr('id');
            var player_key = player_id.split('audiojs_wrapper')[1];
            return {
                el: el_curr,
                player_id: player_id,
                player_key: player_key
            };
        }

    });
    var view_player = new playerView();
})();